
# Usage

## Prerequesites

You need to install

- `docker`
- `docker-compose`

See : 

- https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script Provides several installation methods
  for installing docker. I recommend following the section "Install using the convenience script"

- https://docs.docker.com/compose/install/ The installation guide for `docker-compose`, be sure to follow the instructions
  for linux


## Database usage

- Startup the MySQL server with `docker-compose up`

When launching for the first time, docker compose will create a "volume", which is a virtualized storage unit in which
the database data is persisted. The database data (such as the user and passwords specified in the config file) and the created
tables and data inserted into these tables go into this volume. When you stop and restart the dockerized MySQL server, 
it reuses the volume, so you keep your data from one time to another.

However if you want to delete everything and start again from a clean state, you need to delete the volume. To do so you need
to run the command : `docker-compose down -v` 

If you change the configuration (the name of the default database, the user, the passwords for the user or the root user), 
you need to delete the volume in order for the changes to be taken into account.

- You can connect to the server with the command line `mysql --user root --password="rootpass"  --host 127.0.0.1` 
  (password defined in the `docker-compose.yml` config file)


## Using the mysql CLI client to explore the database server

See the list of mySQL client commands here :  http://g2pc1.bu.edu/~qzpeng/manual/MySQL%20Commands.htm

For example, to show the list of databases :

```
> show databases;
```

To connecto to a database (called `my_database`) :

```
> use my_database;
```

To create a new table `Book` in this database, with a primary key `id` and a text column `title` : 

```
> CREATE TABLE Books (id bigint(255) NOT NULL AUTO_INCREMENT PRIMARY KEY, title VARCHAR(999));
```

To list all the tables of the database :

```
> show tables;
```

To insert a row into the `Book` table

```
> INSERT INTO Books (title) VALUES ('First book');
```

To select all books in the table

```
> SELECT * FROM Books;
```
